# NextGrowth Frontend Engineer - 1st Screening

Welcome to the NextGrowth Frontend Engineer 1st Screening project! This readme file will provide you with essential information about the project, its tech stack, and the features  need to implement.

## Tech Stack

For this project, I used the following technologies:

- **React.js/TypeScript**: The core framework for building the frontend of the application.
- **Bootstrap**: We use Bootstrap for styling and layout.
- **React Router DOM v6**: This library is used for handling routing within the application.
- **Users API**: To fetch users data, we utilize the following API: [https://www.slingacademy.com/article/sample-users-free-fake-api-for-practicing-prototyping/#Get_a_List_of_Users](https://www.slingacademy.com/article/sample-users-free-fake-api-for-practicing-prototyping/#Get_a_List_of_Users)

## Features

To successfully complete this 1st screening, I implement all the features outlined in the following document: [Front-end Engineer 1st Screening Features](https://multivariate.notion.site/Front-end-engineer-1st-screening-f462fe077ef14a77bc59b83e24ef88f6).

Please refer to the provided document for detailed information about each feature, including design specifications, functionality requirements, and any additional guidance.



## Screenshots

Here are some vital screenshots of the application:

![Screenshot 1](https://gitlab.com/pradip1910736/NextGrowth/-/raw/main/public/images/Screenshot%202023-09-10%20at%205.24.09%20PM.png?ref_type=heads)

![Screenshot 2](https://gitlab.com/pradip1910736/NextGrowth/-/raw/main/public/images/Screenshot%202023-09-10%20at%205.26.00%20PM.png?ref_type=heads)


## Getting Started

Before you begin, make sure you have the necessary development tools and environment set up:

1. Ensure you have Node.js and npm (Node Package Manager) installed on your system.
2. Clone this repository to your local machine:

   ```bash
   git clone <repository-url>
